from PySide.QtCore import *
from PySide.QtGui import *

def init(self):
  self.on('advance', setNodeLayout)

def setNodeLayout(self):
  for node in self.showed_nodes.values():
    if node.id in self.pin_nodes:
      continue
    if self.scene.mouseGrabberItem() != node.view:
      newPos = calculateForces(self, node)
      node.view.setPos(newPos)

def calculateForces(self, node):
  item = node.view
  xvel = 0.0
  yvel = 0.0
  for other_node in self.showed_nodes.values():
    other_item = other_node.view
    line = QLineF(item.mapFromItem(other_item, 0, 0),
        QPointF(0, 0))
    dx = line.dx()
    dy = line.dy()
    l = 1 + dx * dx + dy * dy
    xvel += dx * 500 / l
    yvel += dy * 500 / l
  charge = (len(node.edges) + 1) * 2.0
  for edge in node.edges:
    if edge.left == node:
      pos = item.mapFromItem(edge.right.view, 0, 0)
    else:
      pos = item.mapFromItem(edge.left.view, 0, 0)
    additional_charge = int(node.get('layout.add_charge', 0))
    xvel += pos.x() / (charge + additional_charge)
    yvel += pos.y() / (charge + additional_charge)

  if qAbs(xvel) < 0.1 and qAbs(yvel) < 0.1:
    xvel = yvel = 0.0
  return item.pos() + QPointF(xvel, yvel)
