from PySide.QtCore import *
from PySide.QtGui import *

class VFiller(QFrame):
  def __init__(self):
    super(VFiller, self).__init__()
    self.setFrameShape(QFrame.NoFrame)
    policy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
    self.setSizePolicy(policy)
