from PySide.QtCore import *
from PySide.QtGui import *

def init(self):
  self.on('keyPress', onKeyPress)

def onKeyPress(self, event):
  key = event.key()
  if key == Qt.Key_Z:
    scaleView(self, 1.2)
  elif key == Qt.Key_X:
    scaleView(self, 1 / 1.2)

def scaleView(self, scaleFactor):
  factor = self.matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width()
  if factor < 0.07 or factor > 10:
    return
  self.scale(scaleFactor, scaleFactor)
