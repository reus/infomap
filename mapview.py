# coding: utf8

from PySide.QtCore import *
from PySide.QtGui import *
from onfire import Wood

MAPVIEW_MODULES = (
  'spring_layout',
  'scale',
  'scroll',
  )

NODEVIEW_MODULES = (
  'text',
  'block',
  'editor',
  )

EDGEVIEW_MODULES = (
  'link',
  )

class MapView(QGraphicsView, Wood):
  def __init__(self, map, window):
    super(MapView, self).__init__()
    self.map = map
    self.window = window
    self.scene = QGraphicsScene()
    self.setScene(self.scene)

    self.setRenderHint(QPainter.Antialiasing)
    self.setDragMode(QGraphicsView.RubberBandDrag)
    self.setViewportUpdateMode(QGraphicsView.BoundingRectViewportUpdate)
    self.setTransformationAnchor(QGraphicsView.NoAnchor)
    self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)

    self.showed_nodes = {}
    self.showed_edges = {}
    self.pin_nodes = set()

    for module in MAPVIEW_MODULES:
      m = __import__('mapview_' + module)
      m.init(self)

    self.timerId = self.startTimer(1000 / 30)

  def timerEvent(self, event):
    self.fire('advance')

  def viewNode(self, node):
    self.pin_nodes.add(node.id)
    nodes_to_add = [node]
    edges_to_add = []
    while nodes_to_add:
      n = nodes_to_add.pop()
      self.addNodeView(n)
      for edge in n.edges:
        edges_to_add.append(edge)
        if edge.left == n:
          if edge.right.id not in self.showed_nodes:
            nodes_to_add.append(edge.right)
        else:
          if edge.left.id not in self.showed_nodes:
            nodes_to_add.append(edge.left)
    while edges_to_add:
      edge = edges_to_add.pop()
      self.addEdgeView(edge)

  def addNodeView(self, node):
    if node.id not in self.showed_nodes:
      item = NodeView(node, self)
      self.scene.addItem(item)
      item.setPos(qrand() % 700, qrand() % 700)
      self.showed_nodes[node.id] = node

  def addEdgeView(self, edge):
    if edge.id not in self.showed_edges:
      item = EdgeView(edge)
      self.scene.addItem(item)
      self.showed_edges[edge.id] = edge

  def keyPressEvent(self, event):
    self.fire('keyPress', event)

class NodeView(QGraphicsItem, Wood):
  def __init__(self, node, mapview):
    super(NodeView, self).__init__()
    self.setFlag(QGraphicsItem.ItemIsMovable)
    self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
    self.setZValue(10)
    self.node = node
    self.mapview = mapview
    setattr(node, 'view', self)

    for module in NODEVIEW_MODULES:
      m = __import__('nodeview_' + module)
      m.init(self)

    self.fire('init')
    self.on('contentChange', lambda self: self.adjustEdges())

  def boundingRect(self):
    self.rect = QRectF()
    self.fire('boundingRectUnited')
    self.fire('boundingRectResize')
    return self.rect

  def paint(self, painter, option, widget):
    self.fire('paint', painter, option, widget)

  def itemChange(self, change, value):
    if change == QGraphicsItem.ItemPositionHasChanged:
      self.adjustEdges()
    return super(NodeView, self).itemChange(change, value)

  def adjustEdges(self):
    for edge in self.node.edges:
      try:
        edge.view.fire('adjust')
      except AttributeError:
        pass

  def mousePressEvent(self, event):
    self.fire('mousePress', event)
    QGraphicsItem.mousePressEvent(self, event)

  def mouseReleaseEvent(self, event):
    self.fire('mouseRelease', event)
    QGraphicsItem.mouseReleaseEvent(self, event)

class EdgeView(QGraphicsItem, Wood):
  def __init__(self, edge):
    super(EdgeView, self).__init__()
    self.setZValue(-1)
    self.edge = edge
    setattr(edge, 'view', self)
    self.isSelfDirected = edge.left == edge.right

    for module in EDGEVIEW_MODULES:
      m = __import__('edgeview_' + module)
      m.init(self)

    self.fire('init')
    self.fire('adjust')

  def boundingRect(self):
    self.rect = QRectF()
    self.fire('boundingRect')
    return self.rect

  def paint(self, painter, option, widget):
    self.fire('paint', painter, option, widget)
