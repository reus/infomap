class Wood(object):
  def on(self, when, what, *args, **kwargs):
    if not hasattr(self, '_woods'):
      setattr(self, '_woods', {})
    self._woods.setdefault(when, [])
    self._woods[when].append((what, args, kwargs))

  def fire(self, when, *args, **kwargs):
    if not hasattr(self, '_woods'):
      setattr(self, '_woods', {})
    if when not in self._woods:
      return
    for wood in self._woods[when]:
      func, bind_args, bing_kwargs = wood
      kws = bing_kwargs.copy()
      kws.update(kwargs)
      func(self, *(bind_args + args), **kws)
