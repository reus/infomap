# coding: utf8

class MapTextView:
  def __init__(self, map):
    self.map = map

  def showNode(self, node, visited_nodes = None, indent = 0):
    if visited_nodes is None:
      visited_nodes = set()
    print node
    visited_nodes.add(node.id)
    for edge in node.edges:
      if edge.left == edge.right:
        print ' ' * 2 * (indent + 1), '><(%d)' % edge.id
      elif edge.left == node:
        print ' ' * 2 * (indent + 1), '>>(%d)' % edge.id,
        if edge.right.id in visited_nodes:
          print edge.right.id
        else:
          self.showNode(edge.right, visited_nodes, indent + 1)
      elif edge.right == node:
        print ' ' * 2 * (indent + 1), '<<(%d)' % edge.id,
        if edge.left.id in visited_nodes:
          print edge.left.id
        else:
          self.showNode(edge.left, visited_nodes, indent + 1)

def test():
  import map
  m = map.Map(('localhost', 'infomap_test', '', 'infomap_test'))
  n1 = m.newNode()
  n1['text'] = u"N1:大家好啊"
  n2 = m.newNode()
  n2['text'] = 'N2'
  n3 = m.newNode()
  n3['text'] = u'N3:反对萨反对萨分阿斯顿分阿斯顿的发送飞洒地方萨'
  e1 = m.newEdge(n1, n2)
  e2 = m.newEdge(n1, n3)
  e3 = m.newEdge(n2, n3)
  e4 = m.newEdge(n2, n1)
  e5 = m.newEdge(n1, n1)

  view = MapTextView(m)
  view.showNode(n1)

if __name__ == '__main__':
  test()
