from PySide.QtCore import *
from PySide.QtGui import *

def init(self):
  self.on('init', setText)
  self.on('boundingRectUnited', onBoundingRectUnited)
  self.on('contentChange', setText)

def setText(self):
  if hasattr(self, 'textItem'):
    self.mapview.scene.removeItem(self.textItem)
    self.prepareGeometryChange()
  text = self.node.get('text', '')
  if isinstance(text, str):
    text = text.decode('utf8')
  self.textItem = QGraphicsTextItem(text, parent = self)
  self.textItem.setFont(QFont("Monaco", 14))
  textRect = self.textItem.boundingRect()
  self.textRect = textRect
  self.textItem.setPos(-textRect.width() / 2, -textRect.height() / 2)

def onBoundingRectUnited(self):
  self.rect = self.rect.united(self.textRect)
  self.rect.moveCenter(QPointF(0, 0))
