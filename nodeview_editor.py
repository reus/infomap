from PySide.QtCore import *
from PySide.QtGui import *
from utils import VFiller
from onfire import Wood

def init(self):
  self.on('mouseRelease', onMouseRelease)

def onMouseRelease(self, event):
  pos = event.pos()
  downPos = event.buttonDownPos(Qt.RightButton)
  if event.button() == Qt.RightButton:
    if qAbs(pos.x() - downPos.x()) < 10 and qAbs(pos.y() - downPos.y()) < 10:
      showEditor(self)

def showEditor(self):
  if hasattr(self.mapview, 'node_editor'):
    self.mapview.node_editor.close()
  editor = Editor(self)
  self.mapview.node_editor = editor
  self.mapview.window.leftPanel.addWidget(editor)

class Editor(QFrame):
  def __init__(self, nodeview):
    super(Editor, self).__init__()
    self.nodeview = nodeview
    self.layout = QVBoxLayout()
    self.layout.setContentsMargins(0, 0, 0, 0)
    self.setLayout(self.layout)
    self.loadData()

  def loadData(self):
    item = self.layout.takeAt(0)
    while item:
      widget = item.widget()
      if widget:
        widget.deleteLater()
      item = self.layout.takeAt(0)
    self.fields = []
    self.delete_checkboxes = []
    data = extract_data(self.nodeview.node)
    for namespace in sorted(data.keys()):
      group = QGroupBox(namespace)
      self.layout.addWidget(group)
      group_layout = QVBoxLayout()
      group.setLayout(group_layout)
      for key in sorted(data[namespace].keys()):
        row_layout = QHBoxLayout()
        group_layout.addLayout(row_layout)
        checkbox = DeleteCheckbox(self.nodeview.node, namespace, key)
        self.delete_checkboxes.append(checkbox)
        row_layout.addWidget(checkbox)
        row_layout.addWidget(QLabel(key))
        field = ValueField(data[namespace][key])
        self.fields.append(field)
        row_layout.addWidget(field)
        field.on('contentChange', self.onContentChange, self.nodeview, namespace, key)
    self.layout.addWidget(self.controlButtons())
    self.layout.addWidget(VFiller())

  def controlButtons(self):
    widget = QWidget()
    layout = QHBoxLayout()
    widget.setLayout(layout)

    new_button = QPushButton("New")
    new_button.clicked.connect(self.newAttr)
    layout.addWidget(new_button)

    save_button = QPushButton("Save and Delete selected")
    save_button.clicked.connect(self.saveAll)
    layout.addWidget(save_button)

    close_button = QPushButton("Close")
    close_button.clicked.connect(self.close)
    layout.addWidget(close_button)

    return widget

  def newAttr(self):
    dialog = NewAttrDialog(self.nodeview)
    dialog.on('save', lambda dialog: self.loadData())
    dialog.exec_()

  def saveAll(self):
    for field in self.fields:
      if field.layout.currentWidget() == field.edit:
        field.contentChange()
    has_deletiong = False
    for box in self.delete_checkboxes:
      if box.isChecked():
        has_deletiong = True
        if box.namespace == 'attr':
          key = box.key
        else:
          key = box.namespace + '.' + box.key
        del box.node[key]
        box.node.view.fire('contentChange')
    if has_deletiong:
      self.loadData()

  @staticmethod
  def onContentChange(field, nodeview, namespace, key):
    value = field.edit.text()
    if namespace != 'attr':
      key = namespace + '.' + key
    if isinstance(value, unicode):
      value = value.encode('utf8')
    value = type(nodeview.node[key])(value)
    nodeview.node[key] = value
    nodeview.fire('contentChange')

class ValueField(QWidget, Wood):
  def __init__(self, text):
    super(ValueField, self).__init__()
    if not isinstance(text, unicode):
      text = str(text).decode('utf8')
    self.layout = QStackedLayout()
    self.setLayout(self.layout)
    self.label = QLabel(text)
    self.edit = QLineEdit(text)
    self.layout.addWidget(self.label)
    self.layout.addWidget(self.edit)
    self.label.mouseDoubleClickEvent = self.labelClick
    self.setFixedHeight(20)
    self.setSizePolicy(QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed))
    self.edit.returnPressed.connect(self.contentChange)

  def switch(self):
    if self.layout.currentWidget() == self.label:
      self.layout.setCurrentWidget(self.edit)
    else:
      self.layout.setCurrentWidget(self.label)

  def labelClick(self, event):
    self.switch()

  def contentChange(self):
    text = self.edit.text()
    self.label.setText(text)
    self.switch()
    self.fire('contentChange')

class NewAttrDialog(QDialog, Wood):
  def __init__(self, nodeview):
    super(NewAttrDialog, self).__init__()
    self.nodeview = nodeview
    self.setSizePolicy(QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum))
    layout = QVBoxLayout()
    self.setLayout(layout)
    self.key_field = QLineEdit()
    layout.addWidget(self.key_field)
    self.value_field = QLineEdit()
    layout.addWidget(self.value_field)
    ok_button = QPushButton("Save")
    layout.addWidget(ok_button)
    cancel_button = QPushButton("Cancel")
    layout.addWidget(cancel_button)

    cancel_button.clicked.connect(self.close)
    ok_button.clicked.connect(self.save)

  def save(self):
    key = self.key_field.text().encode('utf8')
    value = self.value_field.text().encode('utf8')
    if key:
      self.nodeview.node[key] = value
      self.nodeview.fire('contentChange')
    self.fire('save')
    self.close()

class DeleteCheckbox(QCheckBox):
  def __init__(self, node, namespace, key):
    super(DeleteCheckbox, self).__init__()
    self.node = node
    self.namespace = namespace
    self.key = key

def extract_data(node):
  namespaces = {}
  for key, value in node.iteritems():
    key = key.split('.', 1)
    if len(key) > 1:
      namespace = key[0]
      name = key[1]
    else:
      namespace = 'attr'
      name = key[0]
    namespaces.setdefault(namespace, {})
    namespaces[namespace][name] = value
  return namespaces
