# coding: utf8

import pyuse
import doclayer

def _test_map():
  return Map(DbStorage('localhost', 'infomap_test', '', 'infomap_test'))

class Map:
  def __init__(self, storage):
    self.storage = storage
    self.loaded_nodes = {}
    self.loaded_edges = {}

  def newNode(self):
    '''>>> m = _test_map()
    >>> n = m.newNode()
    >>> n.id > 0
    True
    >>> isinstance(n, Node)
    True
    >>> n.id in m.loaded_nodes
    True
    >>> n['foo'] = 'foo'
    '''
    node_id = self.storage.newNode()
    node = Node(node_id)
    node.storage = self.storage
    self.loaded_nodes[node_id] = node
    return node

  def getNode(self, id):
    '''>>> m = _test_map()
    >>> n1 = m.newNode()
    >>> n1['foo'] = 'foo'
    >>> n2 = m.getNode(n1.id)
    >>> n1 is n2
    True
    >>> n2['foo']
    'foo'
    >>> m.getNode(-1) is None
    True
    >>> m2 = _test_map() # not cached
    >>> n2 = m2.getNode(n1.id)
    >>> n2 == n1
    True
    >>> n2 is n1
    False
    >>> n2['foo']
    'foo'
    '''
    if id in self.loaded_nodes:
      node = self.loaded_nodes[id]
    else:
      node = Node(id)
      node.storage = self.storage
      attrs = self.storage.getNodeAttr(id)
      if attrs is None:
        return None
      node.update(attrs)
    return node

  def findNode(self, namespace = 'attr', *conditions):
    '''>>> m = _test_map()
    >>> n = m.newNode()
    >>> n['foo'] = 'FOO'
    >>> n['bar.bar'] = 'BAR'
    >>> n['baz.baz'] = 'BAZ'
    >>> f = m.findNode('attr', "foo='FOO'", 'order by id desc')
    >>> f is n
    True
    >>> f = m.findNode('bar', "bar='BAR'", 'order by id desc')
    >>> f is n
    True
    >>> f = m.findNode('baz', "baz='BAZ'", 'order by id desc')
    >>> f is n
    True
    '''
    node_ids = self.storage.findNodeIds(namespace, *conditions)
    if node_ids:
      return self.getNode(node_ids[0])
    return None

  def newEdge(self, left, right):
    '''>>> m = _test_map()
    >>> left = m.newNode()
    >>> right = m.newNode()
    >>> edge = m.newEdge(left, right)
    >>> edge.id > 0
    True
    >>> edge.left == left
    True
    >>> edge.right == right
    True
    >>> left.edges[-1] == edge
    True
    >>> right.edges[-1] == edge
    True
    '''
    edge_id = self.storage.newEdge(left, right)
    edge = Edge(edge_id, left, right)
    edge.storage = self.storage
    left.edges.append(edge)
    right.edges.append(edge)
    self.loaded_edges[edge_id] = edge
    return edge

  def getEdge(self, id):
    '''>>> m = _test_map()
    >>> n1 = m.newNode()
    >>> n2 = m.newNode()
    >>> e = m.newEdge(n1, n2)
    >>> e['foo'] = 'foo'
    >>> e2 = m.getEdge(e.id)
    >>> e is e2
    True
    >>> e2['foo']
    'foo'
    >>> m.getEdge(-1) is None # not exists
    True
    >>> m2 = _test_map() # not cached
    >>> e2 = m2.getEdge(e.id)
    >>> e2 is e
    False
    >>> e2 == e
    True
    >>> e2['foo'] # autosync
    'foo'
    '''
    if id in self.loaded_edges:
      edge = self.loaded_edges[id]
    else:
      left, right, attrs = self.storage.getEdge(id)
      if left is None:
        return None
      left = self.getNode(left)
      right = self.getNode(right)
      edge = Edge(id, left, right)
      edge.storage = self.storage
      edge.update(attrs)
    return edge

  def findEdge(self, namespace, *conditions):
    '''>>> m = _test_map()
    >>> n1 = m.newNode()
    >>> n2 = m.newNode()
    >>> e = m.newEdge(n1, n2)
    >>> e['foo'] = 'foo'
    >>> e['bar.bar'] = 'bar'
    >>> e['baz.baz'] = 'baz'
    >>> res = m.findEdge('attr', "foo='foo'", 'order by id desc')
    >>> res is e
    True
    >>> res = m.findEdge('bar', "bar='bar'", 'order by id desc')
    >>> res is e
    True
    >>> res = m.findEdge('baz', "baz='baz'", 'order by id desc')
    >>> res is e
    True
    '''
    ids = self.storage.findEdgeIds(namespace, *conditions)
    if ids:
      return self.getEdge(ids[0])
    return None

class DbStorage(doclayer.Db):
  def __init__(self, *args, **kwargs):
    doclayer.Db.__init__(self, *args, **kwargs)

  def newNode(self):
    doc = self.new('node')
    doc['namespaces'] = ''
    doc.save()
    return doc.doc_id

  def getNodeAttr(self, id):
    doc = self.get('node', id)
    if not doc:
      return None
    namespaces = doc['namespaces'].split('|')
    attrs = self._getNamespaceAttrs('node', id, namespaces)
    return attrs

  def syncNode(self, node):
    self.syncEntity(node, 'node')

  def findNodeIds(self, namespace, *conditions):
    node_ids = self.select('id', 'from `node.%s`' % namespace, *conditions)
    node_ids = map(lambda x: x['id'], node_ids)
    return node_ids

  def newEdge(self, left, right):
    doc = self.new('edge')
    doc['left'] = left.id
    doc['right'] = right.id
    doc['namespaces'] = ''
    doc.save()
    return doc.doc_id

  def getEdge(self, id):
    doc = self.get('edge', id)
    if not doc:
      return None, None, None
    namespaces= doc['namespaces'].split('|')
    attrs = self._getNamespaceAttrs('edge', id, namespaces)
    return doc['left'], doc['right'], attrs

  def findEdgeIds(self, namespace, *conditions):
    ids = self.select('id', 'from `edge.%s`' % namespace, *conditions)
    ids = map(lambda x: x['id'], ids)
    return ids

  def syncEdge(self, edge):
    self.syncEntity(edge, 'edge')

  def syncEntity(self, entity, type):
    doc = self.get(type, entity.id)
    namespace_values = {}
    for k, v in entity.iteritems():
      k = k.split('.', 1)
      if len(k) > 1:
        namespace = k[0]
        attr_name = k[1]
      else:
        namespace = 'attr'
        attr_name = k[0]
      namespace_values.setdefault(namespace, {})
      namespace_values[namespace][attr_name] = v
    doc['namespaces'] = '|'.join(namespace for namespace in namespace_values)
    doc.save()
    for namespace, values in namespace_values.iteritems():
      namespace_doc = self.new('%s.%s' % (type, namespace), integer = ['id'])
      namespace_doc.unique('id')
      namespace_doc['id'] = entity.id
      namespace_doc.update(values)
      namespace_doc.save()

  def _getNamespaceAttrs(self, prefix, id, namespaces):
    attrs = {}
    for namespace in namespaces:
      if not namespace: continue
      doc = self.find('%s.%s' % (prefix, namespace), 'id=%d' % id)
      for k, v in doc.iteritems():
        if k != 'id':
          if namespace == 'attr':
            attrs[k] = v
          else:
            attrs['%s.%s' % (namespace, k)] = v
    return attrs

class DumbStorage:
  def __init__(self): pass
  def newNode(self): pass
  def getNodeAttr(self, id): pass
  def syncNode(self, node): pass
  def newEdge(self, left, right): pass
  def getEdgeAttr(self, id): pass
  def syncEdge(self, edge): pass

class Node(dict):
  def __init__(self, id):
    super(Node, self).__init__()
    self.id = id
    self.edges = []
    self.storage = DumbStorage()

  def __eq__(self, o):
    return self.id == o.id

  def __str__(self):
    return '<Node %d [%d edges]> %s' % (
        self.id, len(self.edges),
        super(Node, self).__str__())

  def __setitem__(self, *args, **kwargs):
    super(Node, self).__setitem__(*args, **kwargs)
    self.storage.syncNode(self)

class Edge(dict):
  def __init__(self, id, left, right):
    super(Edge, self).__init__()
    self.id = id
    self.left = left
    self.right = right
    if left.id == right.id:
      left.edges.append(self)
    else:
      left.edges.append(self)
      right.edges.append(self)
    self.storage = DumbStorage()

  def __eq__(self, o):
    return self.id == o.id

  def __str__(self):
    return '<Edge %d [left %d] [right %d]> %s' % (
        self.id, self.left.id, self.right.id,
        super(Edge, self).__str__())

  def __setitem__(self, *args, **kwargs):
    super(Edge, self).__setitem__(*args, **kwargs)
    self.storage.syncEdge(self)

if __name__ == '__main__':
  import doctest
  doctest.testmod()
