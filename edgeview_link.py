from PySide.QtCore import *
from PySide.QtGui import *

def init(self):
  self.on('adjust', onAdjust)
  self.on('boundingRect', onBoundingRect)
  self.on('paint', onPaint)

def onAdjust(self):
  if self.isSelfDirected:
    adjust_point(self)
  else:
    adjust_line(self)
  self.prepareGeometryChange()

def adjust_point(self):
  item = self.edge.left.view
  self.setPos(self.mapToScene(self.mapFromItem(item, item.boundingRect().topRight())) + QPointF(-10, 10))

def adjust_line(self):
  left = self.edge.left.view
  right = self.edge.right.view
  startPoint = left.pos()
  endPoint = right.pos()
  self.line = QLineF(startPoint, endPoint)

  startRect = self.mapToScene(self.mapFromItem(left, left.boundingRect()))
  startIntersected = False
  for l in rect_lines(startRect):
    intersectType, startIntersectPoint = l.intersect(self.line)
    if intersectType == QLineF.BoundedIntersection:
      startIntersected = True
      break
  if startIntersected:
    startPoint = startIntersectPoint
    self.line = QLineF(startIntersectPoint, endPoint)

  endRect = self.mapToScene(self.mapFromItem(right, right.boundingRect()))
  endIntersected = False
  for l in rect_lines(endRect):
    intersectType, endIntersectPoint = l.intersect(self.line)
    if intersectType == QLineF.BoundedIntersection:
      endIntersected = True
      break
  if endIntersected:
    self.line = QLineF(startPoint, endIntersectPoint)

  self.setPos(self.line.pointAt(0.5))

def rect_lines(rect):
  yield QLineF(rect[0], rect[1])
  yield QLineF(rect[1], rect[2])
  yield QLineF(rect[2], rect[3])
  yield QLineF(rect[3], rect[4])

def onBoundingRect(self):
  if self.isSelfDirected:
    self.rect = QRectF(-10, -10, 20, 20)
    return
  rect = QRectF(self.line.p1(), self.line.p2()).normalized()
  rect.setSize(rect.size() + QSizeF(20, 20))
  rect.moveCenter(QPoint(0, 0))
  self.rect = rect

def onPaint(self, painter, option, widget):
  painter.setPen(QPen(Qt.blue, 2))
  painter.setBrush(Qt.blue)
  painter.setOpacity(0.5)
  if self.isSelfDirected:
    painter.drawEllipse(self.mapFromScene(self.pos()), 3, 3)
  else:
    p1 = self.mapFromScene(self.line.p1())
    p2 = self.mapFromScene(self.line.p2())
    painter.drawLine(p1, p2)
    painter.drawEllipse(p2, 3, 3)
