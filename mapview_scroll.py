from PySide.QtCore import *
from PySide.QtGui import *

def init(self):
  self.on('keyPress', onKeyPress)

def onKeyPress(self, event):
  key = event.key()
  pixel_per_move = 200
  if key == Qt.Key_W:
    self.translate(0, pixel_per_move)
  elif key == Qt.Key_S:
    self.translate(0, -pixel_per_move)
  elif key == Qt.Key_A:
    self.translate(pixel_per_move, 0)
  elif key == Qt.Key_D:
    self.translate(-pixel_per_move, 0)
