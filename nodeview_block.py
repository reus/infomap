from PySide.QtCore import *
from PySide.QtGui import *

def init(self):
  self.on('init', onInit)
  self.on('boundingRectResize', onBoundingRectResize)
  self.on('paint', onPaint)

def onInit(self):
  self.vspace = 10
  self.hspace = 20

def onBoundingRectResize(self):
  self.rect.setSize(self.rect.size() + QSizeF(self.hspace * 2, self.vspace * 2))
  self.rect.moveCenter(QPointF(0, 0))

def onPaint(self, painter, option, widget):
  painter.setBrush(Qt.black)
  painter.setPen(Qt.NoPen)
  painter.setOpacity(0.05)
  painter.drawRect(self.rect)
