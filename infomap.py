#!/usr/bin/env python2
# coding: utf8
from PySide.QtCore import *
from PySide.QtGui import *
from mapview import MapView
import sys
import map

class InfoMap(QWidget):
  def __init__(self, map):
    super(InfoMap, self).__init__()

    self.layout = QHBoxLayout()
    self.layout.setSpacing(0)
    self.layout.setContentsMargins(0, 0, 0, 0)
    self.setLayout(self.layout)

    self.leftPanelWidget = QFrame()
    self.leftPanel = QVBoxLayout()
    self.leftPanelWidget.setLayout(self.leftPanel)
    self.layout.addWidget(self.leftPanelWidget)

    self.mapview = MapView(map, self)
    self.layout.addWidget(self.mapview)

def main():
  app = QApplication(sys.argv)
  qsrand(QTime(0, 0, 0).msecsTo(QTime.currentTime()))

  m = map.Map(map.DbStorage('localhost', 'infomap_test', '', 'infomap_test'))
  infomap = InfoMap(m)

  ns = []
  for i in range(9):
    n = m.newNode()
    n['text'] = 'N%d' % i
    ns.append(n)
  m.newEdge(ns[0], ns[1])
  m.newEdge(ns[1], ns[2])
  m.newEdge(ns[0], ns[3])
  m.newEdge(ns[4], ns[1])
  m.newEdge(ns[3], ns[4])
  m.newEdge(ns[4], ns[5])
  m.newEdge(ns[2], ns[5])
  m.newEdge(ns[3], ns[6])
  m.newEdge(ns[4], ns[7])
  m.newEdge(ns[5], ns[8])
  m.newEdge(ns[6], ns[7])
  m.newEdge(ns[7], ns[8])

  n1 = m.newNode()
  n1['text'] = "N1:大家好啊"
  n1['layout.add_charge'] = 20
  n2 = m.newNode()
  n2['text'] = 'N2'
  n2['layout.add_charge'] = 20
  n3 = m.newNode()
  n3['text'] = 'N3:反对萨反对萨分阿斯顿分阿斯顿的发送飞洒地方萨'
  n3['layout.add_charge'] = 20
  e1 = m.newEdge(n1, n2)
  e1['text'] = 'E1'
  e2 = m.newEdge(n1, n3)
  e2['text'] = 'E2'
  e3 = m.newEdge(n2, n3)
  e3['text'] = 'E3'
  e4 = m.newEdge(n2, n1)
  e4['text'] = 'E4'
  e5 = m.newEdge(n1, n1)
  e5['text'] = 'E5'
  m.newEdge(n1, ns[0])

  infomap.mapview.viewNode(n1)

  infomap.show()
  sys.exit(app.exec_())

if __name__ == '__main__':
  main()
